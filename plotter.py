"""Plotter Module for genome sequence and its P-index and I-index values
Author: Krister Jazz Urog
Date: May 7, 2019
"""

import numpy as np
import matplotlib.pyplot as plt
import reading_module_version as rmv
plt.style.use('ggplot')

def plot_interactive(sequences_dict, genome_wt):
	"""
	Plots an interactive plot for comparison of genome sequences/
	-----------
	Parameters:
		sequences_dict: dictionary of genome sequences
		genome_wt : basis genome for comparison
	-----------
		Shows an interactive matplotlib figure 
	"""
	keys = sequences_dict.keys()
	global fig
	fig, ax = plt.subplots(figsize=(20,4))
	print("Length of sequence is ", len(genome_wt))
	sequence1 = sequences_dict['wt']
	print("Length of values is ", len(sequence1))
	x_axis1 = range(len(sequence1))

	ax.fill_between(range(len(genome_wt)), -2.62, 2.62, color='y', alpha = 0.25)
	line1, = ax.plot(list(x_axis1), sequence1, label='wt')

	for i, protein in enumerate(genome_wt):
		ax.annotate(protein, (i,1), xycoords = ('data','axes points'))

	ax.set_xticklabels([])
	ax.set_ylim(-10,10)
	ax.set_xlabel('Genome sequence')
	ax.set_ylabel('Index values')


	sequence2 = sequences_dict['M11x']
	x_axis2 = range(len(sequence2))
	line2, = ax.plot(list(x_axis2), sequence2, label='M11x')

	sequence3 = sequences_dict['M11']
	x_axis3 = range(len(sequence3))
	line3, = ax.plot(list(x_axis3), sequence3, label='M11')

	sequence4 = sequences_dict['M7']
	x_axis4 = range(len(sequence4))
	line4, = ax.plot(list(x_axis4), sequence4, label='M7')

	sequence5 = sequences_dict['M30']
	x_axis5 = range(len(sequence5))
	line5, = ax.plot(list(x_axis5), sequence4, label='M30')

	leg = ax.legend(loc='upper left', fancybox=True, shadow=True)
	leg.get_frame().set_alpha(0.4)

	# we will set up a dict mapping legend line to orig line, and enable
	# picking on the legend line
	lines = [line1, line2, line3, line4, line5]
	global lined

	lined = dict()

	for legline, origline in zip(leg.get_lines(), lines):
		legline.set_picker(5)  # 5 pts tolerance
		lined[legline] = origline

	fig.canvas.mpl_connect('pick_event', onpick)

	plt.show()

def onpick(event):
    	# on the pick event, find the orig line corresponding to the
    	# legend proxy line, and toggle the visibility
	legline = event.artist
	origline = lined[legline]
	vis = not origline.get_visible()
	origline.set_visible(vis)
	# Change the alpha on the line in the legend so we can see what lines
	# have been toggled
	if vis:
		legline.set_alpha(1.0)
	else:
		legline.set_alpha(0.2)
	fig.canvas.draw()


def get_plot_data(sequences, octamers_db):
	"""Gets all the aggregate data from the sequences
	Parameters:
		sequences = genome sequence in string format
		octamers_db = database of p-index and i-index values for each genome
	---------------------------
	Returns:
		Dictionary of aggregated data with key=Genome name, value= Genome sequence
	"""
	data_dict = dict()
	for key in sequences.keys():
		sequence = sequences[key]
		p_index, i_index = rmv.return_indexes(octamers_db, sequence)
		aggregate = aggregate_values(p_index, i_index)
		data_dict[key] = aggregate

	return data_dict

from matplotlib.widgets import TextBox
from reading_module_version import update, return_indexes
from matplotlib.widgets import CheckButtons

def plot(sequence_name, sequence, p_values, i_values):
	"""Saves a figure of P-index and I-index of a genome sequence)
	Parameters:
		sequence = genome sequence in string format
		p_values = pre-computed P-index of the sequence
		i_values = pre-computed I-index of the sequence
	---------------------------
	Returns:
		Saves a result.png figure in the current directory
	"""
	initial_text = sequence
	
	assert type(sequence)==type(str()), "Input sequence must be a string"
	assert(len(p_values) == len(i_values)), "P-index list must be of equal length to I-index list"
	assert(len(sequence) == len(p_values)+8), "Indexes length must be 8 less than sequence length"

	x_axis = range(len(sequence))


	####Plotting part
	fig, ax1 = plt.subplots(figsize=(20,4))
	#P-index and I-index plot
	#ax1.plot(list(x_axis[:-8]), p_values, c='b', label='P-index')
	#ax1.plot(list(x_axis[:-8]), i_values, c='m', label='I-index')
	ax1.fill_between(x_axis, -2.62, 2.62, color='y', alpha = 0.25)

	aggregate = aggregate_values(p_values, i_values)
	l0,= ax1.plot(list(x_axis[:-8]), aggregate, c='c', label=sequence_name)

	#ax1.axvline(x=8, color='k')
	plt.subplots_adjust(left=0.25)
	rax=plt.axes([0.05, 0.4, 0.1, 0.15])
	check = CheckButtons(rax, [sequence_name], [True])
        

################ user input seq ################
	def submit(text):
		
		x,y= update(text)
		l1,=ax1.plot(list(x_axis[:-8]), aggregate_values(x, y), label='Input seq.')

		plt.draw()
      

	axbox = plt.axes([0.15, 0.009, 0.8, 0.05])
	text_box = TextBox(axbox, 'Input seq.', initial=initial_text)
	text_box.on_submit(submit)

############## checkbox   #####################
	def func(label):
		if label == sequence_name[1:]:
			l0.set_visible(not l0.get_visible())	

		plt.draw()	
	check.on_clicked(func)

	
	#Plotting enchancer and silencer points
	for i in range(len(aggregate)):
		if aggregate[i] > 2.62:
			ax1.axvline(x=i, color='g', linestyle='--')
		elif aggregate[i]< -2.62:
			ax1.axvline(x=i, color='r', linestyle='--')
	#Annotating the sequence to figure
	for i, protein in enumerate(sequence):
		ax1.annotate(protein, (i,1), xycoords = ('data','axes points'))
	ax1.set_xticklabels([])

	#Axis labels
	ax1.set_ylim(-10,10)
	ax1.grid(False)
	ax1.set_xlabel('Genome sequence')
	ax1.set_ylabel('Index values')
	ax1.legend()
	fig.savefig('result.png', dpi=500)
	plt.show(fig)

def analyze_values(p_values, i_values):
	"""Creates a list of location of enhancers and silencers based
	on p_values and i_values of a sequence
	============================================
	Parameters:
		p_values: P-index values
		i_values: I-index values
	---------------------------------------------
	Returns:
		enhancers: list of enhancer location
		silencers: list of silencer location
	"""
	assert len(p_values)==len(i_values), "P-index must be of the same size as I-index"

	enhancers = []
	silencers = []
	for i in range(len(p_values)):
		if p_values[i] >2.62 and i_values[i]>2.62:
			enhancers.append(1)
			silencers.append(0)
		elif p_values[i]<-2.62 and i_values[i]<-2.62:
			silencers.append(1)
			enhancers.append(0)
		else:
			enhancers.append(0)
			silencers.append(0)

	return(enhancers, silencers)

def aggregate_values(p_values, i_values):
	"""Creates a list of aggregate values of p-index
	and i_index of a sequence
	============================================
	Parameters:
		p_values: P-index values
		i_values: I-index values
	---------------------------------------------
	Returns:
		aggregate: aggregated values 
	"""	
	aggregate = []
	for i in range(len(p_values)):
		if abs(p_values[i]) > abs(i_values[i]):
			aggregate.append(i_values[i])
		else:
			aggregate.append(p_values[i])

	return aggregate
