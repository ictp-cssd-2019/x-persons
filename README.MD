
Mutation Analyser
=================

This is a repository intended for development of the 'Mutation Analyser' for the 8-mer given Genome sequence defined in an input file 'test.fasta'.
the project make the user flexible with of read the Genome seqences from the input file or input new sequnce in the interactive graph.


The file with the extension .ipynb are Jupyter notebook files and serve as examples for using the framework. For users who have no experience with the Jupyter notebook, it is recommended to read sections 1.1 and 3 from this [tutorial](https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/what_is_jupyter.html#notebook-document).


Installation
============

- Navigate to the directory where you want the software to be installed and clone this repository.
- Use python 3.x
- pip install numpy
- pip install pandas
- pip install matplotlib
- Wait for the script to be finished
- Be sure that you but the input files in the correct path (same script path)
- Check the installation by running the example notebook.

Mutation Analyser guidance
==========================
Full documantation for the prject [here]


Usefull material
================
- https://www.ncbi.nlm.nih.gov/pubmed/15145827
- https://genome.cshlp.org/content/8/11/1142.full
- https://onlinelibrary.wiley.com/doi/pdf/10.1002/humu.10077
- https://jmg.bmj.com/content/36/10/730







