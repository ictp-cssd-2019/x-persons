Aditional Information:
======================

This is a repository indended for developement of the 'Mutation Analyser' for the 8-mer given Genome sequence defined in an input file 'test.fasta'. the project make the user flexible with of read the the Genome sequences from the input file or input new sequence in the interactive graph.

The file with the extension .ipynb are Jupyter notebook files and serve as examples for using the framework. For user who have no experience with the Jupyter notebook, it is recommended to read sections 1.1 and 3 from this [tutorial](https://jupyter-notebook-beginner-guide.readt-hedocs.io/en/lastest/what_is_jupyter.html#notebook-document).

.. include:: README.md

