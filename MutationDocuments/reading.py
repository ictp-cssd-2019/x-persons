import numpy as np
import pandas as pd

octamers_file = pd.read_csv('octamers.txt',
names = ['octamer', 'P', 'I'], sep = '\t', skiprows = 24, index_col = False)

with open('test.fasta') as file1:
    test_file = file1.readlines()

def return_indexes(sequence):
    '''
    returns the P and I indexes of the octamers a given nucleotide sequence, looping through the sequence
    and returning the indexes for each iterator
    
    Parameters
    ----------
    sequence : str
    
    Returns
    ----------
    out : lists
    '''
    p_index_list, i_index_list = [], []
    for seq_index in range(0,len(sequence) - 7):
        p_index_list.append(octamers_file['P'][octamers_file['octamer'] == sequence[seq_index:seq_index + 8].lower()])
        i_index_list.append(octamers_file['I'][octamers_file['octamer'] == sequence[seq_index:seq_index + 8].lower()])
    return p_index_list, i_index_list

def setting_sequences(seq_list):
    '''
    sets the sequence in the test.fasta file in a correct way so they can be plotted correctly
    
    Parameters
    ----------
    seq_list : list of sequences from the test.fasta file
    
    Returns
    ----------
    out : lists, seq_names and seq_strings
    '''
    seq_names, seq_strings = [],[]
    for i in seq_list:
        if '>' in i:
            seq_names.append(i.split('\n'))
        else:
            seq_strings.append(i[4:-6])
    return seq_names, seq_strings
    
return_indexes(setting_sequences(test_file)[1][0])
