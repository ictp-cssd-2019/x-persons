import numpy as np
import pandas as pd
import re

def return_indexes(octamers_db, sequence):
    '''
    returns the P and I indexes of the octamers a given nucleotide sequence, looping through the sequence
    and returning the indexes for each iterator

    Parameters
    ----------
    sequence : str

    Returns
    ----------
    out : lists
    '''
    assert(type(octamers_db)== type(pd.DataFrame()) ), "Input should be a DataFrame"
    p_list = []
    i_list = []
    for i in range(len(sequence)-8):
        seq_to_find = sequence[i:i+8].lower()
        p_index , i_index = find_values(seq_to_find, octamers_db)
        p_list.append(p_index)
        i_list.append(i_index)

    return(p_list, i_list)

def find_values(sequence, octamers_db):
#    print(octamers_db)
    P = np.float32(octamers_db[octamers_db['octamer']==sequence]['P'])
    I = np.float32(octamers_db[octamers_db['octamer']==sequence]['I'])
    return P, I

def setting_sequences(seq_list):
    '''
    sets the sequence in the test.fasta file in a correct way so they can be plotted correctly
    
    Parameters
    ----------
    seq_list : list of sequences from the test.fasta file
    
    Returns
    ----------
    out : lists, seq_names and seq_strings
    '''
    
    seq_dict = {}
    
    seq_names, seq_strings = [],[]
    for i,j in enumerate(seq_list):
        if '>' in j:
            seq_names.append(re.split('>|\n', j)[1])
        else:
            seq_strings.append(j[4:-6])

        
    for i,j in zip(seq_names, seq_strings):
        seq_dict[i] = j
    
    del seq_names, seq_strings
    return seq_dict


def update (sequence):
    octamers_dp = pd.read_csv('octamers.txt',
            names = ['octamer', 'P', 'I'], sep = '\t', skiprows = 24, index_col = False)
    p_values, i_values = return_indexes(octamers_dp, sequence)
    
    return p_values, i_values
