.. Mutation_Analiser documentation master file, created by
   sphinx-quickstart on Wed May  8 12:11:52 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mutation Analyser
=================
Welcome to Mutation Analyser. Here you can check protein binding strength for different genome sequences, making comparisons between different sequences or editing a sequence for nucleotides.
Below you can find links for two pages: one of them is comparative genome sequence plot and the other one is the genome sequence editor. On the pages themselves you will find more information about them. Down here you will find some information on exons/introns and protein binding strength. There is also a link for the code documentation (in Python 3.5) that we used to create the plots.
Eukaryotic genes have coding regions (exons) interrupted by non-coding introns. Exons must be selected and spliced together and introns removed. This process is governed by numerous short exonic sequences called splicing enhancers (ESE) or splicing silencers (ESS). The balance between ESE and ESS determines whether an exons is selected or not. Unsurprisingly, mutations in ESE or ESS sequences can cause human disease due to loss or gain of the exon. Your task is to create an interactive mutation analyser that will help biologists to predict the loss or gain of ESE or ESS sequences.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   code_interactive
   reading

.. figure:: ../MutationDocuments/fig1.png
   :align: center

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


**Autors:**
*Luis Salles, Paola Nazate, Yassmin Mahmoud, Jazz Orug*
