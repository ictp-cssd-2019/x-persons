import reading_module_version as rmv
import plotter
import numpy as np
import pandas as pd


def comparative_plot(sequence_file, octamers_db):
	basis_genome = sequence_file['wt']
	sequence_dict = plotter.get_plot_data(sequence_file, octamers_db)
	plotter.plot_interactive(sequence_dict, basis_genome)

def interactive_plot(sequence_file, octamers_db):
	keys = sequence_file.keys()
	print('Choose sequence from this keys: ', list(keys))
	chosen_key = input(" ")

	chosen_sequence = sequence_file[chosen_key]
	print(chosen_sequence)

	p_values, i_values  = rmv.return_indexes(octamers_db,chosen_sequence)

	plotter.plot(chosen_key,chosen_sequence, p_values, i_values)

def main():
	octamers_db = pd.read_csv('octamers.txt',
	names = ['octamer', 'P', 'I'], sep = '\t', skiprows = 24, index_col = False)

	with open('test.fasta') as file1:
    		test_file = file1.readlines()

	sequence_file = rmv.setting_sequences(test_file)

	not_option = True
	while not_option:
		option = input("Which plot do you want to see?\n 1) Comparative plot 2) Interactive plot 3) quit ")
		if option =='1':
			comparative_plot(sequence_file, octamers_db)
			not_option=False
		elif option=='2':
			interactive_plot(sequence_file, octamers_db)
			not_option = False
		elif option =='3':
			print("Thank you! See you later")
			break
		else:
			print("\nPlease choose among the options.")

main()
