import pytest
import reading_module_version as rmv
import pandas as pd


def test_return_index_valid():
	valid_input = pd.DataFrame()
	assert( type(valid_input) == type(pd.DataFrame()))

def test_reader_invalid_input():
	invalid_input = 'asdfsf'
	with pytest.raises(AssertionError):
		rmv.return_indexes(invalid_input, invalid_input)

def test_reader_invalid_output():
	pass


def test_parser_valid():
	pass

def test_parser_invalid_input():
	pass

def test_parser_invalid_output():
	pass
