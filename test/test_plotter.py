import plotter

import pytest

import os
import numpy as np


def test_analyze_values_invalid():
	with pytest.raises(AssertionError):
		plotter.analyze_values([1,2,3],[1,2])

def test_analyze_values_input():
	with pytest.raises(TypeError):
		plotter.analyze_values(0,1)

def test_analyze_values_output_length():
	n = 20
	p_dummy = np.random.randint(0,10,size=n)
	i_dummy = np.random.randint(0,10,size=n)

	result = plotter.analyze_values(p_dummy, i_dummy)

	assert(len(result[0])==n)
	assert(len(result[1])==n)

def test_analyze_values_enhancer_output():
	n = 20
	p_dummy = np.random.randint(0,10,size=n)
	i_dummy = np.random.randint(0,10,size=n)

	result = plotter.analyze_values(p_dummy, i_dummy)

	for enhancer in result[0]:
		assert(enhancer==1 or enhancer==0)

def test_analyze_values_silencer_output():
	n = 20
	p_dummy = np.random.randint(0,10,size=n)
	i_dummy = np.random.randint(0,10,size=n)

	result = plotter.analyze_values(p_dummy, i_dummy)

	for silencer in result[1]:
		assert(silencer==1 or silencer==0)

	result = plotter.analyze_values(p_dummy, i_dummy)


def test_plot_wrong_sequence_input():
	with pytest.raises(AssertionError):
		plotter.plot('a',1, [1,2,3], [1,2,3])

def test_plot_wrong_input_lengths():
	with pytest.raises(AssertionError):
		plotter.plot('a','a', [1,2,3], [1,2])

def test_plot_wrong_input_lengths2():
	with pytest.raises(AssertionError):
		plotter.plot('a','a', [1,2,3], [1,2,3])

def test_plot_output():
	s = 'asdfssdfsadfdsadsaf'
	p_valid = np.random.random(len(s)-8)
	i_valid = np.random.random(len(s)-8)

	plotter.plot(s,s,p_valid, i_valid)
	exists = os.path.isfile('result.png')

	if exists:
        	pass
	else:
		raise("File not found")
